import os
import subprocess
from threading import Thread
from time import sleep
import sys
import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib

nRuns = 100
data_folder = "./data/"
function = "SchaffersEvaluation"
plt.figure()
matplotlib.scale.LogTransform
plt.xlabel("Generations")
plt.ylabel("Best fitness")

experiment_name = "ps_50_mpr_0.10_np_2_ts_4_es_3_copr_1.00_mnstd_1.00_ct_AverageCrossover_st_Tournament_me_false_ng_false__ngs_4__ngl_100__nruns_100_"
f = open(data_folder + function + "/" + experiment_name + "/finalResult.txt", "r")
averages = [float(i) for i in f.readline().split(",")]
bests = [float(i) for i in f.readline().split(",")]
f.close()
plt.plot(bests, "k", label = "Baseline")

members = [2, 2.5, 3, 3.5, 4, 4.5]
colours = ["g", "b", "r", "c", "m", "y"]


for i in range(6):
    experiment_name = "ps_50_mpr_0.10_np_2_ts_4_es_3_copr_1.00_mnstd_1.00_ct_AverageCrossover_st_Tournament_me_false_ng_true__ngs_" + str(members[i]) + "__ngl_100__nruns_100_"
    f = open(data_folder + function + "/" + experiment_name + "/finalResult.txt", "r")
    averages = [float(i) for i in f.readline().split(",")]
    bests = [float(i) for i in f.readline().split(",")]
    f.close()
    plt.plot(bests, colours[i], label = "No-go size: " + str(members[i]))
plt.legend()
plt.savefig("graph/result.png")