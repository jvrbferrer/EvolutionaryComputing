# Evolutionary Computing Project
In order to run this program on a linux-based computer you need to go through the following steps:
1. In file "compile.sh" in the first line, change the address to the address of the folder where you unzipped the project.
2. In file "run.py" in line 25, you can change the function to "SchaffersEvaluation", "KatsuuraEvaluation" and "BentCigarFunction".
3. In file "run.py" between lines 26 to 28 you can change between the 3 different algorithms(proposed augmentations).
4. Go to the project directory and run the python script with: python run.py
5. You can find the outputs in ./data

# Note that all the parameters we modify are in run.py