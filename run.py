import os
import subprocess
from threading import Thread
from time import sleep
import sys
import math
import numpy as np
import matplotlib.pyplot as plt

def run_experiment(programName, fileName):
    print(programName)
    counter = 0
    result = subprocess.run(programName, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    while result.stderr != None and result.stderr != "":
        counter += 1
        if counter == 10:
            print(result.stderr)
            return
        result = subprocess.run(programName, encoding='utf-8', stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    f = open(fileName, 'w')
    f.write(result.stdout)
    f.close()


function = "SchaffersEvaluation"
#playerType = "NogoPlayer";
playerType = "ProbabilisticNogoPlayer";
#playerType = "JumpPlayer";

populationSize = 50       #50; ps
mutationProb = 0.1          #0.1; mpr
numberOfParents = 2         #2; np
tournamentSize = 4         #4; ts
elitismSize = 3             #3; es
crossoverProb = 1           #1;copr
mutNoiseStd = 1             #1; mnstd
crossoverType = "AverageCrossover"    #"1Point"; ct
selectionType = "Tournament"#"Tournament"; st
mutateEqual = "false"       #0; me
nogo = "false"
if(playerType=="NogoPlayer" or playerType=="ProbabilisticNogoPlayer"):
    nogo = "true"
nogoSize = 4
nogoLimit = 100

nRuns = 10
nThreads = 8
threads = []

data_folder = "./data/"
experiment_name_template = "ps_%d_mpr_%0.2f_np_%d_ts_%d_es_%d_copr_%0.2f_mnstd_%0.2f_ct_%s_st_%s_me_%s_ng_%s__ngs_%s__ngl_%s__nruns_%s_"
experiment_name = experiment_name_template%(populationSize, mutationProb, numberOfParents, tournamentSize, elitismSize, crossoverProb, mutNoiseStd, crossoverType, selectionType, mutateEqual, nogo, nogoSize, nogoLimit, nRuns)

completed_process = subprocess.run(["bash","./compile.sh"], encoding='utf-8', stderr=subprocess.PIPE)
if completed_process.stderr != None and completed_process.stderr != "":
    print(completed_process.stderr)
    sys.exit()

os.makedirs(data_folder + function + "/" + experiment_name)
parameters_definition = "-DpopSize=%d -DmutProb=%f -DnParents=%d -DtournSize=%d -DelSize=%d -DcoProb=%f -DmutStd=%f -DmutEqual=%s -DcoType=%s -DselType=%s -Dnogo=%s -DnogoSize=%s -DnogoLimit=%s" 
parameters_definition = parameters_definition%(populationSize, mutationProb, numberOfParents, tournamentSize, elitismSize, crossoverProb, mutNoiseStd, mutateEqual, crossoverType, selectionType, nogo, nogoSize, nogoLimit)

for t in range(nRuns):
    thread = Thread(target=run_experiment, args=(("java " + parameters_definition + " -jar testrun.jar -submission=" + playerType + " -evaluation=" + function + " -seed="+str(t)).split(' '), data_folder + function + "/" + experiment_name + "/run"+str(t)+".txt"))
    thread.start()
    threads.append(thread)
    sleep(2.5)

for t in threads:
    t.join()


#ANALISE DATA AND SAVE A PICTURE OF THE PLOT AND A FILE WITH THE RESULTS
best_by_generation_per_run = []
mean_by_generation_per_run = []
std_by_generation_per_run = []
nogo_by_generation_per_run = []
nogo_list_by_generation_per_run = []
best_per_run = []

for t in range(nRuns):
    f = open(data_folder + function + "/" + experiment_name + "/run"+str(t)+".txt", "r")
    std_genes_per_run = []
    fitnesses_per_run = []
    best_fitnesses_per_run = []
    nogo_per_run = []
    nogo_list_per_run = []

    while True:
        #Per generation
        best_fitness_so_far = 0
        fitnesses = []
        genes_pop = []
        nogo_collisions = 0
        nogo_list = 0
        gen_line = f.readline()
        gen_line = gen_line.split(",")
        if len(gen_line) != 2:
            break
        for i in range(int(gen_line[1])):
            #Per individual
            ind = f.readline()
            ind = ind.split(",")
            genotype = []
            for g in range(10):
                genotype.append(float(ind[g]))
            fitnesses.append(float(ind[10]))
            genes_pop.append(genotype)
        genes_pop = np.asarray(genes_pop)
        std_genes = np.std(genes_pop, axis=0)
        if(nogo == "true"):
            nogo_collisions = int(f.readline());
            nogo_list = int(f.readline());

        ##RESULT
        std_genes = np.mean(std_genes)
        max_fitness = max(fitnesses)
        average_fitness = sum(fitnesses) / int(gen_line[1])
        std_genes_per_run.append(std_genes)
        best_fitnesses_per_run.append(max_fitness)
        fitnesses_per_run.append(average_fitness)
        nogo_per_run.append(nogo_collisions)
        nogo_list_per_run.append(nogo_list)
    best_by_generation_per_run.append(best_fitnesses_per_run)
    best_per_run.append(best_fitnesses_per_run[-1])
    mean_by_generation_per_run.append(fitnesses_per_run)
    std_by_generation_per_run.append(std_genes_per_run)
    nogo_by_generation_per_run.append(nogo_per_run);
    nogo_list_by_generation_per_run.append(nogo_list_per_run);

mean_by_generation = np.mean(mean_by_generation_per_run, axis=0)
best_by_generation = np.mean(best_by_generation_per_run, axis=0)
std_by_generation = np.mean(std_by_generation_per_run, axis=0)
nogo_by_generation = np.mean(nogo_by_generation_per_run, axis=0)
nogo_list_by_generation = np.mean(nogo_list_by_generation_per_run, axis=0)
f = open(data_folder + function + "/" + experiment_name + "/finalResult.txt", "w")
f.write(",".join([str(i) for i in mean_by_generation]) + "\n")
f.write(",".join([str(i) for i in best_by_generation]) + "\n")
f.write(",".join([str(i) for i in std_by_generation]) + "\n")
if (nogo == "true"):
    f.write(",".join([str(i) for i in nogo_by_generation]) + "\n")
    f.write(",".join([str(i) for i in nogo_list_by_generation]) + "\n")
f.write("mean: " + str(np.mean(best_per_run)))
f.write(" sd: " + str(np.std(best_per_run)))
f.close()

f = open(data_folder + function + "/" + experiment_name + "/finalResult.txt", "r")
averages = [float(i) for i in f.readline().split(",")]
bests = [float(i) for i in f.readline().split(",")]
diversities = [float(i) for i in f.readline().split(",")]
if (nogo == "true"):
    nogos = [float(i) for i in f.readline().split(",")]
    nogo_lists = [float(i) for i in f.readline().split(",")]
f.close()

plt.figure()
plt.plot(averages, "y")
plt.plot(bests, "g")
plt.plot(diversities, "r")
plt.text(0, 0, "Best Individual = " + str(bests[-1]))
plt.savefig(data_folder + function + "/" + experiment_name + "/result.png")

if (nogo == "true"):
    plt.figure()
    plt.plot(nogos, "y")
    plt.savefig(data_folder + function + "/" + experiment_name + "/nogo_plot.png")

    plt.figure()
    plt.plot(nogo_lists, "y")
    plt.savefig(data_folder + function + "/" + experiment_name + "/nogo_list_plot.png")