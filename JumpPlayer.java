import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.util.Random;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.lang.Math;

public class JumpPlayer implements ContestSubmission
{
    Random rnd_;
    ContestEvaluation evaluation_;
    private int evaluations_limit_;

    // Parameters
    private int populationSize = 50;
    private double mutationProb = 0.1;
    private double mutNoiseStd = 1;
    private double mutNoiseFac = 1;
    private boolean verbose = false;
    private double jumpLength = 0.5;
    private int tournamentSize;

    public boolean evaluationsEnded = false;

    public JumpPlayer()
    {
        rnd_ = new Random();
    }

    public void setSeed(long seed)
    {
        // Set seed of algortihms random process
        rnd_.setSeed(seed);
    }

    public void setEvaluation(ContestEvaluation evaluation)
    {
//        System.out.println("TEST");
        // Set evaluation problem used in the run
        evaluation_ = evaluation;

        // Get evaluation properties
        Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
        // Property keys depend on specific evaluation
        // E.g. double param = Double.parseDouble(props.getProperty("property_name"));
        boolean isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
        boolean hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
        boolean isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

        //Read parameters:
        this.populationSize = Integer.parseInt(System.getProperty("popSize"));
        this.mutationProb = Double.parseDouble(System.getProperty("mutProb"));
        this.tournamentSize = Integer.parseInt(System.getProperty("tournSize"));
        this.mutNoiseStd = Double.parseDouble(System.getProperty("mutStd"));

        if (System.getProperty("jLength") == null) jumpLength = 0.5; //Alpha or Jump Length
        else jumpLength = Double.parseDouble(System.getProperty("jLength"));

        if (props.getProperty("verbose") == null) verbose = true;
        else this.verbose = Boolean.parseBoolean(props.getProperty("verbose"));

        // Do sth with property values, e.g. specify relevant settings of your algorithm
        if(isMultimodal){
            // Do sth
        } else{
            // Do sth else
        }
        Ind.evaluation_ = evaluation_;
        Ind.rnd_ = rnd_;
        //TODO add gloablas of ind
    }

    public void run() {
        // Run your algorithm here
        double best_score = 0;
        int evals = populationSize;
        // Initialize population and fill it with random Inds
        ArrayList<Ind> population = new ArrayList<Ind>(populationSize);
        for(int i = 0; i < populationSize; i++) {
            Ind ind = new Ind();
            ind.evaluate();
            population.add(ind);
        }
        Collections.sort(population);
        //System.err.println(population);
        int nGenerations = 0;
        while(evals<evaluations_limit_){
            nGenerations++;
            this.mutNoiseStd *= this.mutNoiseFac;
            for (int i = 0; i < 10; i++) {
                evals++;//population.size();
                addChild(population);
            }

            printPopulation(nGenerations, population);
        }
    }

    void addChild(ArrayList<Ind> population){

        //Generate children
        ArrayList<Ind> tournament = tournamentSelection(population, tournamentSize);
        Ind pW = tournament.get(0);
        Ind pB = tournament.get(tournament.size()-1);
        Ind child = new Ind();

        for (int i = 0; i <10; i++){
            double pBi = pB.getGene(i);
            double pWi = pW.getGene(i);
            double v = (pBi - pWi) * (jumpLength + rnd_.nextGaussian()) * mutNoiseFac ;
            v = Math.max(Math.min(v, 5),-5);
            child.setGene(i, v+pBi);
        }

        //Mutate
        addNoiseMutateInd(child, rnd_);
        child.evaluate();
        killInsert(population, child);

    }


    void killInsert(ArrayList<Ind> pop, Ind ind){
        if(ind.compareTo(pop.get(0)) < 0) return;
        pop.set(0, ind);
        for(int i = 1; i < pop.size() && ind.compareTo(pop.get(i)) > 0; i++){
            Collections.swap(pop, i-1, i);
        }

    }

    void addNoiseMutateInd(Ind ind, Random rnd_) {
        for(int i=0; i<10; i++) {
            if(rnd_.nextDouble() < mutationProb) {
                double newGene = ind.getGene(i) + rnd_.nextGaussian() * mutNoiseStd;
                if(newGene > 5.0) {
                    newGene = 5.0;
                }
                else if(newGene < -5.0) {
                    newGene = -5.0;
                }
                ind.setGene(i, newGene);
            }
        }
    }

    void printPopulation(int generation, ArrayList<Ind> pop) {
        System.out.println(Integer.toString(generation) + ", " + Integer.toString(pop.size()));

        for(Ind p : pop) {
            String individual = "";
            for(int i=0; i<10; i++) {
                individual += Double.toString(p.getGene(i)) + ",";
            }
            individual += Double.toString(p.getFitness());
            System.out.println(individual);
        }
    }

    ArrayList<Ind> tournamentSelection(ArrayList<Ind> population, int tournamentSize) {
        ArrayList<Ind> tournamentPopulation = new ArrayList<Ind>(tournamentSize);
        for(int u=0; u<tournamentSize; u++) {
            //It's possible to have the same individual in the same tournament
            tournamentPopulation.add(population.get(rnd_.nextInt(population.size())));
        }
        Collections.sort(tournamentPopulation);
        return tournamentPopulation;
    }


}

class Ind implements Comparable<Ind> {
    protected double genotype[];
    protected Double fitness;
    protected boolean isEvaluated;
    public static ContestEvaluation evaluation_;
    public static Random rnd_;

    Ind() {
        genotype = new double[10];
        for(int i=0; i<10; i++) {
            genotype[i] = rnd_.nextDouble();
        }
    }

    Ind(double[] genes) {
        genotype = genes;
    }

    public void setGene(int i, double newValue) {
        this.genotype[i] = newValue;
    }

    public double getGene(int i) {
        return this.genotype[i];
    }

    public double getFitness() {
        return this.fitness;
    }

    public void evaluate(){
        fitness = (Double)evaluation_.evaluate(genotype);
    }

    @Override
    public int compareTo(Ind i) {
        double iFit = i.getFitness();
        double myFit = this.getFitness();
        if(myFit > iFit) {
            return 1;
        }
        else if(myFit < iFit) {
            return -1;
        }
        return 0;
    }

    @Override
    public String toString(){
        return Double.toString(fitness);
    }

}

