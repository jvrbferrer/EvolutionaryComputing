import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.util.Random;
import java.util.Properties;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class ProbabilisticNogoPlayer implements ContestSubmission
{
    Random rnd_;
    ContestEvaluation evaluation_;
    private int evaluations_limit_;

    // Parameters
    private int populationSize = 50;
    private double mutationProb = 0.1;
    private int numberOfParents = 2;
    private int tournamentSize = 4;
    private int elitismSize = 3;
    private double crossoverProb = 1;
    private double mutNoiseStd = 1;
    private String crossoverType = "1Point";
    private String selectionType = "Tournament";
    private boolean mutateEqual = false; 
    private boolean nogo = false;
    private double nogo_size = 2;
    private int nogo_limit = 100;

    public boolean evaluationsEnded = false;
    
    public ProbabilisticNogoPlayer()
    {
        rnd_ = new Random();
    }
    
    public void setSeed(long seed)
    {
        // Set seed of algortihms random process
        rnd_.setSeed(seed);
    }

    public void setEvaluation(ContestEvaluation evaluation)
    {
        // Set evaluation problem used in the run
        evaluation_ = evaluation;
        
        // Get evaluation properties
        Properties props = evaluation.getProperties();
        // Get evaluation limit
        evaluations_limit_ = Integer.parseInt(props.getProperty("Evaluations"));
        // Property keys depend on specific evaluation
        // E.g. double param = Double.parseDouble(props.getProperty("property_name"));
        boolean isMultimodal = Boolean.parseBoolean(props.getProperty("Multimodal"));
        boolean hasStructure = Boolean.parseBoolean(props.getProperty("Regular"));
        boolean isSeparable = Boolean.parseBoolean(props.getProperty("Separable"));

        //Read parameters:
        this.populationSize = Integer.parseInt(System.getProperty("popSize"));
        this.mutationProb = Double.parseDouble(System.getProperty("mutProb"));
        this.numberOfParents = Integer.parseInt(System.getProperty("nParents"));
        this.tournamentSize = Integer.parseInt(System.getProperty("tournSize"));
        this.elitismSize= Integer.parseInt(System.getProperty("elSize"));
        this.crossoverProb = Double.parseDouble(System.getProperty("coProb"));
        this.mutNoiseStd = Double.parseDouble(System.getProperty("mutStd"));
        this.mutateEqual = Boolean.parseBoolean(System.getProperty("mutEqual"));
        this.crossoverType = System.getProperty("coType");
        this.selectionType = System.getProperty("selType");
        this.nogo = Boolean.parseBoolean(System.getProperty("nogo"));
        this.nogo_size = Double.parseDouble(System.getProperty("nogoSize"));
        this.nogo_limit = Integer.parseInt(System.getProperty("nogoLimit"));
        //System.out.println(crossoverType);

        // Do sth with property values, e.g. specify relevant settings of your algorithm
        if(isMultimodal){
            // Do sth
        } else{
            // Do sth else
        }
    }
    
    public void run() {
        // Run your algorithm here
        double best_score = 0;
        int evals = 0;
        // Initialize population and fill it with random individuals
        double total_initial_fitness = 0;
        ArrayList<Individual> population = new ArrayList<Individual>(populationSize);
        for(int i = 0; i < populationSize; i++) {
            Individual new_individual = new Individual(rnd_, evaluation_);
            population.add(new_individual);
            total_initial_fitness += new_individual.getFitness();
        }
        double average_initial_fitness = total_initial_fitness/populationSize;
        

        ArrayList<Individual> taboo_list = new ArrayList<Individual>();
        for(int i = 0; i < populationSize; i++) {
            if (population.get(i).getFitness() < average_initial_fitness)
                taboo_list.add(population.get(i));
        }


        int nGenerations = 0;
        while(evals<evaluations_limit_ - populationSize){
            int violation_counter = 0;
            nGenerations++;
            //System.out.println(parents.size());
            evals += population.size();
            //Generate children
            ArrayList<Individual> children = new ArrayList<Individual>();
            while(children.size() < populationSize) {
                ArrayList<Individual> newChildren;
                ArrayList<Individual> parents;
                if(this.selectionType.equals("Tournament")) {
                    parents = tournamentSelection(population, this.numberOfParents, this.tournamentSize);
                }
                else {
                    throw new java.lang.Error("Can't recognise selection method: " + this.selectionType);
                }
                // Apply crossover / mutation operators
                if(rnd_.nextDouble() < crossoverProb) {
                    if(this.crossoverType.equals("1Point")) {
                        newChildren = this.onePointCrossover(parents);
                    }
                    else if(this.crossoverType.equals("AverageCrossover")) {
                        newChildren = this.averageCrossover(parents);
                    }
                    else {
                        throw new java.lang.Error("Can't recognise crossover method: " + this.crossoverType);
                    }
                }
                else {
                    newChildren = parents;
                }
                //newChildren = parents;
                for(Individual c : newChildren) {

                    // Repeat mutation if we end up in a no go zone
                    double original_genotype[] = new double[10];
                    for (int i = 0; i < 10; i++)
                        original_genotype[i] = c.getGene(i);
                    this.addNoiseMutateIndividual(c, rnd_);
                    
                    if (nogo)
                    {
                        boolean accepted = false;
                        int nogo_counter = 0;

                        while(!accepted && nogo_counter < nogo_limit)
                        {
                            accepted = true;
                            for (Individual t : taboo_list)
                            {
                                if(nogo_size == 0) {
                                    break;
                                }
                                double distanceToNogo = distance(c.getGenotype(), t.getGenotype());
                                double probabilityOfAcceptance = distanceToNogo / nogo_size;
                                if (rnd_.nextDouble() > probabilityOfAcceptance) {
                                    accepted = false;
                                    violation_counter++;
                                    for (int i = 0; i < 10; i++)
                                        c.setGene(i, original_genotype[i]);
                                    this.addNoiseMutateIndividual(c, rnd_);
                                }
                            }
                            nogo_counter++;
                        }
                    }         
                }
                children.addAll(newChildren);
            }

            // Update taboolist
            for(Individual c : children){
                if (c.getFitness() < average_initial_fitness){
                    taboo_list.add(c);
                }
            }

            // Select parents
            Collections.sort(population);
            ArrayList<Individual> elite = new ArrayList<Individual>();
            for(int i=0; i<elitismSize; i++) {
                elite.add(population.get(i));
            }
            population = children;
            population.subList(0, elite.size()).clear();
            population.addAll(elite);
            if(this.mutateEqual) {
                mutateEquals(population);
            }
            //System.out.println(nGenerations);
            printPopulation(nGenerations, population);
            System.out.println(violation_counter);
            System.out.println(taboo_list.size());
        }
    }

    public double distance(double genotype1[], double genotype2[])
    {
        double sum_of_squares = 0;
        for(int i=0; i<10; i++)
            sum_of_squares += Math.pow(genotype1[i] - genotype2[i], 2);
        return Math.sqrt(sum_of_squares);
    }

    void mutateEquals(ArrayList<Individual> population) {
        Collections.sort(population, new GeneticComparator());
        for(int i=0; i<population.size()-1; i++) {
            boolean areEqual = true;
            for(int g=0; g<10; g++) {
                if(population.get(i).getGene(g) != population.get(i+1).getGene(g)) {
                    areEqual = false;
                    break;
                }
            }
            if(areEqual) {
                addNoiseMutateIndividual(population.get(i), rnd_);
            }
        }
    }
    
    ArrayList<Individual> tournamentSelection(ArrayList<Individual> population, int numberOfParents, int tournamentSize) {
        ArrayList<Individual> newParents = new ArrayList<Individual>(numberOfParents);
        for(int i=0; i<numberOfParents; i++) {
            ArrayList<Individual> tournamentPopulation = new ArrayList<Individual>(tournamentSize);
            for(int u=0; u<tournamentSize; u++) {
                //It's possible to have the same individual in the same tournament
                tournamentPopulation.add(population.get(rnd_.nextInt(population.size())));
            }
            newParents.add(this.getFittest(tournamentPopulation));
        }
        return newParents;
    }

    Individual getFittest(ArrayList<Individual> population) {
        double bestFitnessSoFar = -1;
        Individual bestIndividualSoFar = null;

        for(Individual i : population) {
            if(i.getFitness() > bestFitnessSoFar) {
                bestFitnessSoFar = i.getFitness();
                bestIndividualSoFar = i;
            }
        }
        return bestIndividualSoFar;
    }

    ArrayList<Individual> uniformCrossover(ArrayList<Individual> parents) {
        ArrayList<Individual> children = new ArrayList<Individual>(parents.size());
        //System.out.println(parents.size());
        for(int c=0; c<parents.size(); c++) {
            Individual child = new Individual( rnd_, evaluation_);
            for(int x=0; x<10; x++) {
                child.setGene(x, parents.get(rnd_.nextInt(parents.size())).getGene(x));
            }
            children.add(child);
        }
        return children;
    }

    ArrayList<Individual> onePointCrossover(ArrayList<Individual> parents) {
        ArrayList<Individual> children = new ArrayList<Individual>(parents.size());
        ArrayList<Integer> crossoverPoints = new ArrayList<Integer>();
        int lastPoint = 1;
        for(int i=0; i<parents.size()-1; i++) {
            crossoverPoints.add(rnd_.nextInt(10-lastPoint) + lastPoint);
        }
        for(int c=0; c<parents.size(); c++) {
            Individual child = new Individual( rnd_, evaluation_);
            children.add(child);
        }
        int offset = 0;
        for(int i=0; i<10; i++) {
            if(offset < crossoverPoints.size() && i==crossoverPoints.get(offset)) {
                offset++;
            }
            for(int p=0; p<parents.size(); p++) {
                children.get(p).setGene(i, parents.get((p+offset)%parents.size()).getGene(i));
            }
        }
        return children;
    }

    ArrayList<Individual> averageCrossover(ArrayList<Individual> parents) {
        ArrayList<Individual> children = new ArrayList<Individual>();
        Individual child = new Individual(rnd_, evaluation_);
        for(int i=0; i<10; i++) {
            double geneSum = 0;
            for(int p=0; p<parents.size(); p++) {
                geneSum += parents.get(p).getGene(i);
            }
            child.setGene(i, geneSum / parents.size());
        }
        children.add(child);
        return children;
    }

    void randomMutateIndividual(Individual ind, Random rnd_) {
        for(int i=0; i<10; i++) {
            if(rnd_.nextDouble() < mutationProb) {
                ind.setGene(i, rnd_.nextDouble()*10-5);
            }
        }
    }

    void addNoiseMutateIndividual(Individual ind, Random rnd_) {
        for(int i=0; i<10; i++) {
            if(rnd_.nextDouble() < mutationProb) {
                double newGene = ind.getGene(i) + rnd_.nextGaussian() * mutNoiseStd;
                if(newGene > 5.0) {
                    newGene = 5.0;
                }
                else if(newGene < -5.0) {
                    newGene = -5.0;
                }
                ind.setGene(i, newGene);
            }
        }
    }

   void printPopulation(int generation, ArrayList<Individual> pop) {
        System.out.println(Integer.toString(generation) + ", " + Integer.toString(pop.size()));
        
        for(Individual p : pop) {
            String individual = "";
            for(int i=0; i<10; i++) {
                individual += Double.toString(p.getGene(i)) + ",";    
            }
            individual += Double.toString(p.getFitness());
            System.out.println(individual);
        }      
    }
}