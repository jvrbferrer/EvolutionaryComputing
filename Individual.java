import org.vu.contest.ContestSubmission;
import org.vu.contest.ContestEvaluation;

import java.util.Comparator;
import java.util.Random;

public class Individual implements Comparable<Individual> {
    protected double genotype[];
    protected Double fitness;
    protected boolean isEvaluated;
    protected ContestEvaluation evaluation_;

    Individual(Random rnd_, ContestEvaluation evaluation_) {
        this.genotype = new double[10];
        this.isEvaluated = false;
        this.evaluation_ = evaluation_;
        for(int i=0; i<10; i++) {
            genotype[i] = rnd_.nextDouble() * 10 - 5;
        }
    }

    public double[] getGenotype(){
        return this.genotype;
    }

    public void setGenotype(double genotype[]){
        this.genotype = genotype;
    }

    public void setGene(int i, double newValue) {
        this.genotype[i] = newValue;
    }

    public double getGene(int i) {
        return this.genotype[i];
    }

    public double getFitness() {
        if(!isEvaluated) {
            Object newFitness = this.evaluation_.evaluate(this.genotype);
            if(newFitness == null) {
                //number of evaluations have ended
                this.fitness = (double) -1.0;
            }
            else {
                this.fitness = (double) newFitness;
            }
            this.isEvaluated = true;
        }
        return this.fitness;
    }
    
    @Override
    public int compareTo(Individual i) {
        double iFit = i.getFitness();
        double myFit = this.getFitness(); 
        if(iFit < myFit) {
            return -1;
        }
        else if(iFit > myFit) {
            return 1;
        }
        return 0;
    }

}