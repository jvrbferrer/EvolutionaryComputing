import java.util.Comparator;

class GeneticComparator implements Comparator<Individual> {
    @Override
    public int compare(Individual a, Individual b) {
        for(int i=0; i<10; i++) {
            if(a.getGene(i) < b.getGene(i)) {
                return -1;
            }
        }
        return 0;
    }
}